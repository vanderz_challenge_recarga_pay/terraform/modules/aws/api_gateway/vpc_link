resource "aws_api_gateway_vpc_link" "main" {
  name   = var.vpc_link_name
  description = var.vpc_link_description
  target_arns = var.vpc_link_target_arns
}
